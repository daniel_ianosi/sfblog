<?php

namespace App\Controller;

use App\Entity\ProductCategory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends AbstractController
{
    /**
     * @Route("/shop", name="shop")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $mainCategories = $em->getRepository(ProductCategory::class)->findBy(['parent' => NULL]);
        return $this->render('shop/index.html.twig', [
            'mainCategories' => $mainCategories,
        ]);
    }
}
