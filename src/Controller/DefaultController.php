<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\Subscriber;
use App\Form\PostType;
use App\Form\SubscriberType;
use App\Services\Newsletter;
use App\Services\ShoppingCart;
use Doctrine\DBAL\Query\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(Request $request, PaginatorInterface $paginator , ShoppingCart $shoppingCart)
    {
        $entityManager = $this->getDoctrine()->getManager();
        //$posts = $entityManager->getRepository(Post::class)->findBy([],['id'=>'DESC']);
        $categories = $entityManager->getRepository(Category::class)->findAll();
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $entityManager->createQueryBuilder();
        $queryBuilder->select('p')
            ->from(Post::class, 'p');
        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            2/*limit per page*/
        );

        return $this->render('default/index.html.twig', ['shoppingCart'=>$shoppingCart, 'categories' => $categories, 'pagination' => $pagination]);
    }

    /**
     * @Route("/addToCart/{id}/{q}", name="addToCart")
     */
    public function addToCart($id, $q, ShoppingCart $shoppingCart)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($id);
        $shoppingCart->addToCart($product, $q);

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/articol/{id}/{name}", name="post-action")
     */
    public function post($id, $name)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $post = $entityManager->getRepository(Post::class)->find($id);

            if (is_null($post)) {
                throw new \Exception("A post with id:$id does not exist!");
            }
        } catch (\Exception $ex){
            return $this->render('default/error.html.twig', ['errorMessage' => $ex->getMessage()]);
        }

        return $this->render('default/post.html.twig', ['post' => $post]);
    }

    /**
     * @Route("/articol-add", name="post-add-action")
     */
    public function createPost(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();
        }

        return $this->render('default/addPost.html.twig', ['form'=>$form->createView()]);
    }

    /**
     * @Route("/articol-edit/{id}", name="post-edit-action")
     */
    public function editPost(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $entityManager->getRepository(Post::class)->find($id);
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();

            $entityManager->persist($post);
            $entityManager->flush();
        }

        return $this->render('default/addPost.html.twig', ['form'=>$form->createView()]);
    }

    /**
     * @Route("/subscribe", name="subscribe")
     */
    public function subscribe(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $subscriber = new Subscriber();
        $form = $this->createForm(SubscriberType::class, $subscriber);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $subscriber = $form->getData();

            $entityManager->persist($subscriber);
            $entityManager->flush();
        }

        return $this->render('default/subscribe.html.twig', ['form'=>$form->createView()]);
    }

    /**
     * @Route("/unsubscribe/{email}", name="unsubscribe")
     */
    public function unsubscribe(Request $request, $email)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $subscriber = $entityManager->getRepository(Subscriber::class)->findOneBy(['email'=>$email]);
        if ($subscriber){
            $entityManager->remove($subscriber);
            $entityManager->flush();
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/send-newsletters", name="send-newsletters")
     */
    public function sendNewsletters(Request $request, Newsletter $newsletter)
    {
        $service = new Newsletter();
        $service->setMailer(new \Symfony\Bundle\MonologBundle\SwiftMailer());
        $newsletter->sendNewsletter();

        return $this->redirectToRoute('homepage');
    }
}
