<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 3/9/2020
 * Time: 7:21 PM
 */

namespace App\Services;


use App\Entity\Post;
use App\Entity\Subscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Templating\EngineInterface;

class Newsletter
{
    /** @var  \Swift_Mailer */
    private $mailer;

    /** @var  EngineInterface */
    private $twig;

    /** @var  EntityManagerInterface */
    private $entityManager;

    /**
     * Newsletter constructor.
     * @param \Swift_Mailer $mailer
     * @param EngineInterface $twig
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(\Swift_Mailer $mailer, EngineInterface $twig, EntityManagerInterface $entityManager)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }

    public function sendNewsletter()
    {
        $subscribers = $this->entityManager->getRepository(Subscriber::class)->findAll();

        $lastArticle = $this->entityManager->getRepository(Post::class)->findBy([],['id' => 'DESC'],1);

        foreach ($subscribers as $subscriber){

            $message = (new \Swift_Message('Newsletter '.date('Y-m-d')))
                ->setFrom('daniel@ianosi.ro')
                ->setTo($subscriber->getEmail())
                ->setBody($this->twig->renderResponse('default/newsletter.html.twig',
                    [
                        'post'=>$lastArticle[0],
                        'subscriber' => $subscriber
                    ]
                )->getContent(),'text/html');

            $this->mailer->send($message);
        }
    }

    /**
     * @return \Swift_Mailer
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * @param \Swift_Mailer $mailer
     * @return Newsletter
     */
    public function setMailer($mailer)
    {
        $this->mailer = $mailer;
        return $this;
    }

    /**
     * @return EngineInterface
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * @param EngineInterface $twig
     * @return Newsletter
     */
    public function setTwig($twig)
    {
        $this->twig = $twig;
        return $this;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @return Newsletter
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }




}