<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 3/9/2020
 * Time: 8:41 PM
 */

namespace App\Services;


use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ShoppingCart
{
    /** @var  EntityManagerInterface */
    private $entityManager;

    /** @var  SessionInterface */
    private $session;

    /**
     * ShoppingCart constructor.
     * @param EntityManagerInterface $entityManager
     * @param Session $session
     */
    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
    }


    public function addToCart(Product $product, $quantity)
    {
        foreach ($this->getCart()->getCartItems() as $cartItem){
            if ($cartItem->getProduct()===$product){
                $cartItem->setQuantity($cartItem->getQuantity()+$quantity);
                $this->entityManager->persist($cartItem);
                $this->entityManager->flush();
                return;
            }
        }
        $cartItem = new CartItem();
        $cartItem->setProduct($product);
        $cartItem->setQuantity($quantity);
        $cartItem->setCart($this->getCart());
        $this->entityManager->persist($cartItem);
        $this->entityManager->flush();
    }

    public function updateCart(Product $product, $quantity)
    {
        foreach ($this->getCart()->getCartItems() as $cartItem){
            if ($cartItem->getProduct()===$product){
                $cartItem->setQuantity($quantity);
                if ($quantity==0){
                    $this->entityManager->remove($cartItem);
                } else {
                    $this->entityManager->persist($cartItem);
                }

                $this->entityManager->flush();
                return;
            }
        }
    }

    public function removeFromCart(Product $product)
    {
        foreach ($this->getCart()->getCartItems() as $cartItem){
            if ($cartItem->getProduct()===$product){
                $this->entityManager->remove($cartItem);
                $this->entityManager->flush();
                return;
            }
        }
    }

    public function clearCart()
    {
        foreach ($this->getCart()->getCartItems() as $cartItem){
            $this->entityManager->remove($cartItem);
        }
        $this->entityManager->flush();
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        //nu are cart
        if ($this->session->has('cartId')){
            $cart = $this->entityManager->getRepository(Cart::class)->find($this->session->get('cartId'));
        } else {
            $cart = new Cart();
            $this->entityManager->persist($cart);
            $this->entityManager->flush();
            $this->session->set('cartId', $cart->getId());
        }

        return $cart;
    }

    public function getTotal()
    {
        $total = 0;
        foreach ($this->getCart()->getCartItems() as $cartItem){
            $total += $cartItem->getQuantity() * $cartItem->getProduct()->getPrice();
        }
        return $total;
    }




}