<?php

namespace App\Command;

use App\Entity\Post;
use App\Entity\Subscriber;
use App\Services\Newsletter;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BlogNewsletterCommand extends Command
{
    protected static $defaultName = 'blog:newsletter';

    /** @var  Newsletter */
    protected $newsletterService;

    public function __construct(Newsletter $newsletterService)
    {
        $this->newsletterService = $newsletterService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Sends newsletters!!!!MOFO')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->newsletterService->sendNewsletter();

        $io->success('Newsletter sent successfully!');

        return 0;
    }
}
